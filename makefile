RM := rm -rf

XELATEX := xelatex
MKLATEX := latexmk
PANDOC := pandoc

DOC := main.tex
OUTPUT_DIR := .

.PHONY: all xelatex latexmk pandoc clean dist-clean

# default action
all: clean xelatex latexmk

# just builds a pdf
xelatex: $(DOC)
	$(XELATEX) -synctex-on -interaction-nonstopmode -halt-on-error -shell-escape -output-directory=$(OUTPUT_DIR) $<

# runs the build on every file change
latexmk: $(DOC)
	$(MKLATEX) -pvc -f $<

# use pandoc to convert from markdown to tex and embed
pandoc: framed.pdf
embed.tex: $(DOC:.tex=.md)
	$(PANDOC) -f markdown -t latex -o $@ $<
framed.pdf: embed.tex
	$(XELATEX) -synctex-on -interaction-nonstopmode -halt-on-error -shell-escape -output-directory=$(OUTPUT_DIR) framed.tex

# remove the intermediate files
clean:
	$(RM) *.aux *.fdb_latexmk *.fls *.log *.synctex* *_minted-* embed.tex
	$(RM) *.lof *.pyg *.toc *.out *.lol

# remove the rendered PDF
dist-clean: clean
	$(RM) *.pdf
